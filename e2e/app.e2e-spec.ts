import { ThirdAssignmentPage } from './app.po';

describe('third-assignment App', () => {
  let page: ThirdAssignmentPage;

  beforeEach(() => {
    page = new ThirdAssignmentPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
