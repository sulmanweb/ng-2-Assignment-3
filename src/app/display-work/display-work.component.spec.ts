import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayWorkComponent } from './display-work.component';

describe('DisplayWorkComponent', () => {
  let component: DisplayWorkComponent;
  let fixture: ComponentFixture<DisplayWorkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayWorkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayWorkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
