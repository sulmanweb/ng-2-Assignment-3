import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-display-work',
  templateUrl: './display-work.component.html',
  styleUrls: ['./display-work.component.css']
})
export class DisplayWorkComponent implements OnInit {

  displayParagraph: boolean = false;
  logIdentifier: number = 0;
  logArray: Array<number> = [];

  constructor() { }

  ngOnInit() {
  }

  onButtonClick() {
    this.displayParagraph = !this.displayParagraph;
    this.logIdentifier += 1;
    this.logArray.push(this.logIdentifier);
  }

}
